const { 
  readInput, 
  getPartners, 
  getTheatrePartners, 
  writeOutputCSV 
} = require('./utils');

// read the input file
readInput((input) => {
  // read the partners.csv file
  getPartners((partners) => {
      const output = [];
      // loop through each delivery items and find cheapest partner
      input.forEach(row => {
        const { deliveryId, sizeOfDelivery, theatreId } = row;
        
        // get available partners for the theatre
        const theatreData = getTheatrePartners(theatreId, partners);
        let cheapestTotal = 0;
        let cheapestPartner = ''; 

        // loop through each partner and get total cost
        Object.entries(theatreData).forEach(([partnerId, partnerDetails]) => {
            let remainingData = Number(sizeOfDelivery);
            let total = 0;
            partnerDetails.every(partnerData => {
              const {costPerGB, minSlab, maxSlab, minimumCost} = partnerData;
              // if sizeOfDelivery of delivery is lower than least minSbal that partner supports, 
              // then devlivery by partner impossible
              if (sizeOfDelivery < minSlab) return false;

              if (remainingData <= maxSlab) {
                // if remaining data is smaller than maxSlab, add to total and stop iterating for the partner
                const costOfPartner = remainingData * costPerGB;
                total += costOfPartner < minimumCost ? minimumCost : costOfPartner;
                return false;
              } else {
                // if remaining data is greater than maxSlab, add to total and continue iterating
                remainingData = remainingData - maxSlab;
                const costOfPartner = maxSlab * costPerGB;
                total += costOfPartner;
              }
              return true;
            });

            // check the previous cheapest partner and overwrite if applicable
            if (cheapestTotal === 0 || total < cheapestTotal) {
              cheapestTotal = total;
              cheapestPartner = partnerId;
            }
          });
          // add final result as new row for devliveryId
          output.push([deliveryId, cheapestTotal > 0, cheapestPartner, cheapestTotal || ''])
      });

      // console final ouput
      console.log(output);

      // write the output as csv file
      writeOutputCSV(output, 'output1.csv');
  });
});
