//=====================================
//===== UTIL METHODS ==================
//=====================================

const fs = require('fs');
const csv = require('fast-csv');
const path = require('path');

// read csv input file
function readInput(cb) {
    const input = [];
    fs.createReadStream('input.csv')
      .pipe(csv.parse({ headers: ['deliveryId', 'sizeOfDelivery', 'theatreId'] }))
      .on('data', row => input.push(row))
      .on('end', () => cb(input));
};

// convert partners.csv to javascript array of objects
function getPartners(cb) {
  const partners = [];
  fs.createReadStream('partners.csv')
    .pipe(csv.parse({ headers: ['theatreId', 'sizeSlab', 'minimumCost', 'costPerGB', 'partnerId'], renameHeaders: true }))
    .on('data', row => {
      const formattedRow = {};
      Object.entries(row).forEach(([key, value]) => {
        const trimmedValue = value.trim();
        if (key === 'sizeSlab') {
          const [min, max] = trimmedValue.split('-');
          formattedRow.minSlab = Number(min);
          formattedRow.maxSlab = Number(max);
        } else if (key === 'minimumCost' || key === 'costPerGB') {
          formattedRow[key] = Number(trimmedValue);
        } else {
          formattedRow[key] = trimmedValue;
        }
      });
      partners.push(formattedRow)
    })
    .on('end', () => cb(partners));
};

// get list of available partners for the given theatreId
function getTheatrePartners(theatreId, partners) {
  const partnerMap = {};
  partners.forEach(partner => {
    if (partner.theatreId === theatreId) {
      if (!partnerMap[partner.partnerId]) partnerMap[partner.partnerId] = [];
      partnerMap[partner.partnerId].push(partner);
    }
  });
  return partnerMap;
};

function writeOutputCSV(output, fileName) {
  csv.writeToPath(path.resolve(__dirname, fileName), output)
    .on('error', err => console.error(err))
    .on('finish', () => console.log('Done writing.'));
}

module.exports = {
  readInput,
  getPartners,
  getTheatrePartners,
  writeOutputCSV,
};