# QUBE challenge
Given a list of content size and Theatre ID, Find the partner for each delivery where cost of delivery is minimum. If delivery is not possible, mark that delivery impossible.

## Installation
* Install NodeJS.
* Open terminal or cmd and exectute `npm i`

## Execution
* Run `node index.js`, the output can be verified in console or the same will be generated as CSV file with name `output1.csv`
